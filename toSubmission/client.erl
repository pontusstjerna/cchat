-module(client).
-export([handle/2, initial_state/2, contains/2]).
-include_lib("./defs.hrl").

%% inititial_state/2 and handle/2 are used togetger with the genserver module,
%% explained in the lecture about Generic server.


%% Produce initial state
initial_state(Nick, GUIName) ->
    #client_st { gui = GUIName, nick = Nick, serverAtom = disconnected, connected = false, channels = [] }.

%% ---------------------------------------------------------------------------

%% handle/2 handles each kind of request from GUI

%% All requests are processed by handle/2 receiving the request data (and the
%% current state), performing the needed actions, and returning a tuple
%% {reply, Reply, NewState}, where Reply is the reply to be sent to the
%% requesting process and NewState is the new state of the client.

%% Connect to server
handle(St, {connect, Server}) ->
    ServerAtom = list_to_atom(Server),
    Nick = St#client_st.nick,
    case catch genserver:request(ServerAtom, {newClient, self(), Nick}) of
       clientRegistered -> 
            NewSt = St#client_st {serverAtom = ServerAtom, connected = true },
            {reply, ok, NewSt};
        nickTaken -> {reply, {error, nick_taken, "That nick is already connected to the server."}, St};
        _ -> {reply, {error, server_not_reached, "Server not reached!"}, St}
    end;

%% Disconnect from server
handle(St, disconnect) ->
    if
        St#client_st.channels /= [] -> {reply, {error, leave_channels_first, "Channels still active!"}, St};
        St#client_st.connected == false -> {reply, {error, user_not_connected, "You're not even connected..."}, St};
        true ->
            ServerAtom = St#client_st.serverAtom,
            Nick = St#client_st.nick,
            genserver:request(ServerAtom, {removeClient, Nick}),
            NewSt = St#client_st {serverAtom = disconnected, connected = false},
            {reply, ok, NewSt}
    end;

% Join channel
handle(St, {join, Channel}) ->
    Channels = St#client_st.channels,
    Result = contains(Channel, Channels),
    if
        St#client_st.connected == false -> {reply, {error, not_connected, "You're not even ansluten!"}, St};
        Result == true -> {reply, {error, user_already_joined, "You have already joined that channel!"}, St};
        true ->
            NewSt = St#client_st { channels = [Channel|Channels] },
            genserver:request(St#client_st.serverAtom, {joinChannel, self(), Channel, St#client_st.nick}),
            {reply, ok, NewSt}
    end;

%% Leave channel
handle(St, {leave, Channel}) ->
    Channels = St#client_st.channels,
    case contains(Channel,Channels) of
        true ->
            NewSt = St#client_st { channels = lists:delete(Channel, Channels)},
            genserver:request(St#client_st.serverAtom, {leaveChannel, St#client_st.nick, Channel}),
            {reply, ok, NewSt};
        false ->
            {reply, {error, user_not_joined, "You haven't even joined that channel!"}, St}
    end;
    
% Sending messages
handle(St, {msg_from_GUI, Channel, Msg}) ->
    From = St#client_st.nick,
    Channels = St#client_st.channels,
    case contains(Channel,Channels) of
        true -> 
            genserver:request(St#client_st.serverAtom, {msg, Msg, From, Channel}),
            {reply, ok, St};
        false ->
            {reply, {error, user_not_joined, "User has not joined that channel!"}, St}
    end;

%% Get current nick
handle(St, whoami) ->
    Nick = St#client_st.nick,
    {reply, Nick, St} ;

%% Change nick
handle(St, {nick, Nick}) ->
    case St#client_st.connected of
        false -> NewSt = St#client_st { nick = Nick },
                {reply, ok, NewSt};
        true -> {reply, {error, user_already_connected, "User already connected!"}, St}
    end;
    
%% Incoming message
handle(St = #client_st { gui = GUIName }, {incoming_msg, Channel, Name, Msg}) ->
    gen_server:call(list_to_atom(GUIName), {msg_to_GUI, Channel, Name++"> "++Msg}),
    {reply, ok, St}.

contains(_, []) -> false;
contains(Elem, [Elem|_]) -> true;
contains(Elem, [_|Xs]) -> contains(Elem, Xs).