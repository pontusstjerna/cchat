-module(channel).
-export([handle/2, initial_state/1, client/1]).
-include_lib("./defs.hrl").

%% inititial_state/2 and handle/2 are used togetger with the genserver module,
%% explained in the lecture about Generic server.

% Produce initial state
initial_state(ChannelName) ->
    #channel_st{name = ChannelName, clients = []}.
    
%% ---------------------------------------------------------------------------

%% handle/2 handles requests from clients

%% All requests are processed by handle/2 receiving the request data (and the
%% current state), performing the needed actions, and returning a tuple
%% {reply, Reply, NewState}, where Reply is the reply to be sent to the client
%% and NewState is the new state of the server.

%% 
%%

%% Add a client (connect)
handle(St, {newClient, Self, Nick}) ->
    io:fwrite("Channel ~p recieved a new client: ~p\n", [St#channel_st.name,Self]),
    Clients = St#channel_st.clients,
    case getClientFromNick(Clients,Nick) of
        not_found -> 
            NewClientTuple = {spawn(channel, client, [{Self,St#channel_st.name}]),Nick},
            NewState = St#channel_st{clients = [NewClientTuple|Clients]},
            {reply, clientRegistered, NewState};
        _ -> {reply, nickTaken, St}
    end;
    
%% Remove a client (disconnect)
handle(St, {removeClient, Nick}) ->
    io:fwrite("Channel removing ~p\n", [Nick]),
    Clients = St#channel_st.clients,
    NewState = St#channel_st {clients = lists:delete(getClientAndDisconnect(Clients, Nick), Clients)},
    io:fwrite("My clients are: ~p~n", [NewState#channel_st.clients]),
    {reply, removed, NewState};
    
%% Handle incomming msgs
handle(St, {msg, Msg, From}) ->
    Clients = St#channel_st.clients,
    broadcast(Clients, Msg, From),
    {reply, ok, St}.
    
%% Try to get a client from the client list
getClientAndDisconnect([],_) -> not_found;
getClientAndDisconnect([{Pid, Nick}|Clients],NickRequest) ->
    if
        Nick == NickRequest -> 
            Pid ! disconnect,
            {Pid, Nick};
        true -> getClientAndDisconnect(Clients, NickRequest)
    end.

%% Try to get a client from the client list
getClientFromNick([],_) -> not_found;
getClientFromNick([{Pid, Nick}|Clients],NickRequest) ->
    if
        Nick == NickRequest -> {Pid, Nick};
        true -> getClientFromNick(Clients, NickRequest)
    end. 
    
%% Send a message to all the clients
broadcast([],_,_) -> ok;
broadcast([{ClientPid, Nick}|Clients], Msg, From) ->
    if
        Nick /= From -> ClientPid ! {send, Msg, From};
        true -> ok
    end,
    broadcast(Clients, Msg,From).
    
%% Function to spawn in the client processes 
client({ClientPid,Channel}) -> 
    receive
        {send, Msg, From} -> 
            genserver:request(ClientPid, {incoming_msg, Channel, From, Msg}),
            client({ClientPid, Channel});
        disconnect -> ok
    end.