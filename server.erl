-module(server).
-export([handle/2, initial_state/1]).
-include_lib("./defs.hrl").

%% inititial_state/2 and handle/2 are used togetger with the genserver module,
%% explained in the lecture about Generic server.

% Produce initial state
initial_state(_) ->
    #server_st{ clients = [], channels = []}.
    
%% ---------------------------------------------------------------------------

%% handle/2 handles requests from clients

%% All requests are processed by handle/2 receiving the request data (and the
%% current state), performing the needed actions, and returning a tuple
%% {reply, Reply, NewState}, where Reply is the reply to be sent to the client
%% and NewState is the new state of the server.

%% 
%%

%% Add a client (connect)
handle(St, {newClient, Self, Nick}) ->
    io:fwrite("Server recieved a new client: ~p\n", [Self]),
    Clients = St#server_st.clients,
    case getClient(Clients, Nick) of
        not_found -> 
            NewState = St#server_st{clients = [{Self, Nick}|Clients]},
            io:fwrite("My clients are: ~p~n", [NewState#server_st.clients]),
            {reply, clientRegistered, NewState};
        _ -> {reply, nickTaken, St}
    end;
    
%% Remove a client (disconnect)
handle(St, {removeClient, Nick}) ->
    io:fwrite("Server removing ~p\n", [Nick]),
    Clients = St#server_st.clients,
    NewState = St#server_st {clients = lists:delete(getClient(Clients, Nick), Clients)},
    io:fwrite("My clients are: ~p~n", [NewState#server_st.clients]),
    {reply, removed, NewState};
    
%% Remove a client from its channel
handle(St, {leaveChannel, Nick, Channel}) ->
    io:fwrite("Server removing ~p from channel ~p\n", [Nick,Channel]),
    genserver:request(list_to_atom(Channel), {removeClient, Nick}),
    {reply, leftChannel, St};
    
%% Join/add new channel
handle(St, {joinChannel,Pid, Channel, Nick}) ->
    Channels = St#server_st.channels,
    io:fwrite("Channel is: ~p~n", [Channel]),
    Result = contains(Channel, Channels),
    if
        Result == true -> genserver:request(list_to_atom(Channel), {newClient, Pid, Nick}),
        {reply, ok, St};
        true ->
            NewSt = St#server_st { channels = [Channel|Channels] },
            io:fwrite("Channels in the server are :~p~n", [NewSt#server_st.channels]),
            genserver:start(list_to_atom(Channel), channel:initial_state(Channel), fun channel:handle/2),
            genserver:request(list_to_atom(Channel), {newClient, Pid, Nick}),
            {reply, ok, NewSt}
    end;
    
%% Handle incomming msgs
handle(St, {msg, Msg, From, Channel}) ->
    genserver:request(list_to_atom(Channel), {msg, Msg, From}),
    {reply, ok, St}.
    
%% Try to get a client from the client list
getClient([],_) -> not_found;
getClient([{Pid, Nick}|Clients],NickRequest) ->
    if
        Nick == NickRequest -> {Pid, Nick};
        true -> getClient(Clients, NickRequest)
    end. 
    
contains(_, []) -> false;
contains(Elem, [Elem|_]) -> true;
contains(Elem, [_|Xs]) -> contains(Elem, Xs).